#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "md5.h"

const int PASS_LEN=20;        // Maximum any password will be
const int HASH_LEN=33;        // Length of MD5 hash strings

/* ============== Function Instructions ===================== 
1. Open the dictionary file
2. Loop through the dictionary file, one line at a time.
3. Hash each password. 
4. Compare to the target hash.
5. If they match, return the corresponding password.
6. Free up memory?
=================== Function crackHash =================== */
char *crackHash(char *target, FILE *dictionary)
{
    char d[100];
    char buf_pass2[PASS_LEN];
    while (fgets(buf_pass2, PASS_LEN, dictionary) != NULL)  // Start While  buf_pass is disctionary
    {
		strcpy(d,buf_pass2);                // Allocate to password to d
		char *nl = strchr(d, '\n');         // Find new line in array
		if (nl != NULL)                     // if not NULL
			{
			    *nl = '\0';                 // set null
			}
		char *hash = md5(d, strlen(d));     // run the hash algorithm against password
		// compare the hash file passed from main to the hashed value from password file
		int ret = strcmp(hash, target);     // compare the value 
		if (ret == 0)                       // if strcmp values are equal
		{
		    char *b = d;                    // pointer to d
		    return b;                       // return that pointer
		}
		free(hash);
    }
    return NULL;
}


/* ========================= main Instructions ================================ 
1. Open the hash file for reading.
2. For each hash, crack it by passing it to crackHash
3. Display the hash along with the cracked password:
       ex 5d41402abc4b2a76b9719d911017c592 hello
4. Close the hash file
5. Free up any malloc'd memory?

==================================== main ================================== */
int main(int argc, char *argv[])
{
/* =========================== Define Variables ============================= */
    char a[20], b[20], c[100];
    int count = 1;                  // count set to 1 o bypass file name
    int v = 1;                      // holder for calculation
    char buf_pass[PASS_LEN];        // set buf for password
    char buf_hash[HASH_LEN];        // set buf for hash

/* =========================== Start Arg Check ===========================+== */
    if (argc < 3)                   // check for 2 arguments
    {
        printf("Must supply filename, password.txt and hashes.txt\n\n");
        exit(1);                    // Exit if not 2 arguments
    }

/* ======================== Start File Allocation ========================+== */
    for (int i = 1; i < argc-1; i++)
    {
        FILE *fp;                           // output file
        FILE *f = fopen(argv[i], "r");      // input password.txt file
        FILE *h = fopen(argv[i+1], "r");    // input hashes.txt file        
        fp = fopen("hash_output","a");      // open file for appending
        char* p;
//        printf("argc is %d\n", argc);
//        printf("argc is %s\n", argv[0]);
//        printf("argc is %s\n", argv[1]);
//        printf("argc is %s\n", argv[2]);

/* =========================== Start File Check ============================= */        
        if (!f)                             // if not file
        {
            printf("Can't open %s for reading\n", argv[2]);
            exit(1);                        // exit
        }
        
        if (!h)                             // if not file
        {
            printf("Can't open %s for reading\n", argv[3]);
            exit(1);                        // exit
        }

/* =======================  While Loop to Pass Hash ========================= */
        while (fgets(buf_hash, HASH_LEN+1, h) != NULL)  // Start While
        {
			strcpy(a,buf_hash);                  // Allocate to c
			char *nl = strchr(a, '\n');     // Find new line in array
			if (nl != NULL)                 // if not NULL
			{
			    *nl = '\0';                 // set null   *nl = '\0';
			}
			
/* ============ Print the hash, compute and print the password ============= */			
//			printf("Hash [%d] is %s  ", count, a); 

			printf("%s  ", a);              // print the hash from the hash.txt
			fprintf (fp, "%s  ", a);
            strcpy(c, crackHash(a, f));     //pass the hash and password file
            printf("%s\n", c);              // print the returned password
            fprintf (fp, "%s\n", c);
            count++;                        // increment the count
        }                                   // end while
/* ==============================  Clean Up ================================= */        
        fp = fopen("hash_output","w");      // clears the file
        fclose(f);                          // close the input file
        fclose(h);                          // close the input file
        fclose(fp);                         // close the output file
    }                                       // End for
}                                           // End main  

